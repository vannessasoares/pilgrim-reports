from django.contrib import admin
from .models import BookPublisher, Product, Ebook, Audiobook, AppUser, ReadDayEbook, ReadDayAudio, ReadingEbook, ListeningBook

admin.site.register(BookPublisher)
admin.site.register(Product)
admin.site.register(Ebook)
admin.site.register(Audiobook)
admin.site.register(AppUser)
admin.site.register(ReadDayEbook)
admin.site.register(ReadDayAudio)
admin.site.register(ReadingEbook)
admin.site.register(ListeningBook)
