from django.db import models

# Create your models here.

# Editora
class BookPublisher(models.Model):
    publisher_name = models.CharField("Nome da Editora", max_length=100)

    class Meta:
        verbose_name = "publisher"
        verbose_name_plural = "publishers"
        ordering = ['publisher_name',]

    def __str__(self):
        return self.publisher_name

''''

class Contract(models.Model):
    contract_value = models.FloatField("Valor de Contrato") #data do contrato [acho que vira outra classe]
    contract_date = models.DateTimeField(auto_now_add=True, blank=True)
    update_date = models.DateTimeField(auto_now=True, blank=True)

    def __str__(self):
        return 'AT%d: %s' % (self.numero, self.nome)

'''

# Produto é uma classe pai para e-books e audiobooks
class Product(models.Model): # Book
    title = models.CharField("Título do livro", max_length=100)
    isbn = models.CharField("ISBN", max_length=13, unique=True)
    publisher_year = models.IntegerField("Ano de publicação")
    contract_value = models.FloatField("Valor de Contrato") #data do contrato [acho que vira outra classe]
    book_publisher = models.ForeignKey(BookPublisher, on_delete=models.CASCADE, verbose_name='Editora')

    # Melhor que esses atributos virem classe
    author = models.CharField("Autor do livro", max_length=100)
    genre = models.CharField("Gênero literário", max_length=40)


    class Meta:
        verbose_name = "product"
        verbose_name_plural = "products"
        ordering = ['title',]

    def __str__(self):
        return self.title

# Ebook é uma classe filha de produto... mas os atributos estão desatualizados
class Ebook(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name='Produto')
    pages_quantity = models.IntegerField("Quantidade de páginas")
    # Acho que pode ter quantidade de capítulos...

    class Meta:
        verbose_name = "ebook"
        verbose_name_plural = "ebooks"
        ordering = ['product',]

    def __str__(self):
        return self.product

# Audiobook também é uma classe filha de produto
class Audiobook(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, verbose_name='Produto')
    total_time = models.TimeField("Tempo total")
    # Quantidade de capítulos? Se sim.. talvez seja um atributo de produto

    class Meta:
        verbose_name = "audiobook"
        verbose_name_plural = "audiobooks"
        ordering = ['product',]

    def __str__(self):
        return self.product


class AppUser(models.Model):
    name = models.CharField("Nome do usuário", max_length=100)
    #

    class Meta:
        verbose_name = "user"
        verbose_name_plural = "users"
        ordering = ['name',]

    def __str__(self):
        return self.name

class ReadingEbook(models.Model):
    user_name = models.ForeignKey(AppUser, on_delete=models.CASCADE, verbose_name='Nome do usuário')
    ebook = models.ForeignKey(Ebook, on_delete=models.CASCADE, verbose_name='Livro')

    class Meta:
        verbose_name = "leitura de ebook"
        ordering = ['user_name',]

    def __str__(self):
        return str(self.user_name)  + " - " + str(self.ebook)


class ListeningBook(models.Model):
    user_name = models.ForeignKey(AppUser, on_delete=models.CASCADE, verbose_name='Nome do usuário')
    audiobook = models.ForeignKey(Audiobook, on_delete=models.CASCADE, verbose_name='Livro')

    class Meta:
        verbose_name = "tempo de leitura"
        ordering = ['user_name',]

    def __str__(self):
        return str(self.user_name)  + " - " + str(self.audiobook)


class ReadDayEbook(models.Model):
    day = models.DateTimeField("Dia de leitura")
    read_pages = models.IntegerField("Páginas lidas")
    reading = models.ForeignKey(ReadingEbook, on_delete=models.CASCADE, verbose_name='Leitura')

    class Meta:
        verbose_name = "leitura diária"

class ReadDayAudio(models.Model):
    day = models.DateTimeField("Dia de leitura")
    read_pages = models.IntegerField("Páginas lidas")
    reading = models.ForeignKey(ListeningBook, on_delete=models.CASCADE, verbose_name='Leitura')

    class Meta:
        verbose_name = "tempo de leitura diário"
